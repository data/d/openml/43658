# OpenML dataset: Breast-cancer-gene-expression---CuMiDa

https://www.openml.org/d/43658

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset GSE45827 on breast cancer gene expression from CuMiDa

6 classes
54676 genes
151 samples

About
Here we present the Curated Microarray Database (CuMiDa), a repository containing 78 handpicked cancer microarray datasets, extensively curated from 30.000 studies from the Gene Expression Omnibus (GEO), solely for machine learning. The aim of CuMiDa is to offer homogeneous and state-of-the-art biological preprocessing of these datasets, together with numerous 3-fold cross validation benchmark results to propel machine learning studies focused on cancer research. The database make available various download options to be employed by other programs, as well for PCA and t-SNE results. CuMiDa stands different from existing databases for offering newer datasets, manually and carefully curated, from samples quality, unwanted probes, background correction and normalization, to create a more reliable source of data for computational research.
http://sbcb.inf.ufrgs.br/cumida

References

Feltes, B.C.; Chandelier, E.B.; Grisci, B.I.; Dorn, M. (2019) CuMiDa: An Extensively Curated Microarray Database for Benchmarking and Testing of Machine Learning Approaches in Cancer Research. Journal of Computational Biology, 26 (4), 376-386. [https://doi.org/10.1089/cmb.2018.0238]
Grisci, B. I., Feltes, B. C.,  Dorn, M. (2019). Neuroevolution as a tool for microarray gene expression pattern identification in cancer research. Journal of biomedical informatics, 89, 122-133. [https://doi.org/10.1016/j.jbi.2018.11.013]

Inspiration

How to deal with class imbalance for classification?
How to identify the most important genes for the classification of each cancer subtype?
Is it possible to discover subtypes?
How to beat the classification and clustering benchmarks for this dataset listed on the CuMiDa website?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43658) of an [OpenML dataset](https://www.openml.org/d/43658). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43658/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43658/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43658/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

